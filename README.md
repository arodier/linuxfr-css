# LinuxFR CSS

Feuille de style pour le site Linuxfr.org

## Captures d'écran

### Bureau

![Thème clair](screenshots/desktop-clair.png){width=50%}

![Thème sombre](screenshots/desktop-sombre.png){width=50%}

### Téléphone

![Thème clair](screenshots/phone-clair.png){width=25%}

![Thème sombre](screenshots/phone-sombre.png){width=25%}
